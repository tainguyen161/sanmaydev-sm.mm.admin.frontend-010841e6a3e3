export default ({ store, route, redirect }) => {
  const userPermissions = store.getters['user/permissions']
  if (userPermissions.length === 0) { return redirect('/login') }

  let permissionsRequired = []
  for (let i = 0; i < route.meta.length; i++) {
    permissionsRequired = permissionsRequired.concat(route.meta[i].permissions)
  }
  const allowed = permissionsRequired.every(elem => userPermissions.includes(elem))
  if (!allowed) { return redirect('/login') }
}
