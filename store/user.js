import { login, logout } from '@/api/login'

const userState = JSON.parse(localStorage.getItem('mymarket'))
const user = {
  state: {
    permissions: userState ? userState.user.permissions : [],
    info: userState ? userState.user.info : {}
  },
  getters: {
    info: state => state.info,
    permissions: state => state.permissions
  },
  mutations: {
    SET_PERMISSIONS: (state, roles) => {
      state.permissions = roles
    },
    SET_INFO: (state, info) => {
      state.info = info
    }
  },

  actions: {
    login ({ commit }, userInfo) {
      return new Promise((resolve, reject) => {
        login(userInfo).then((response) => {
          const result = response.data
          commit('SET_INFO', result.user)
          commit('SET_PERMISSIONS', result.permissions)
          resolve()
        }).catch((error) => {
          reject(error)
        })
      })
    },

    logout ({ commit, state }) {
      return new Promise((resolve, reject) => {
        logout(state.token).then(() => {
          resolve()
        }).catch((error) => {
          reject(error)
        }).finally(() => {
          commit('SET_INFO', {})
          commit('SET_PERMISSIONS', [])
        })
      })
    }

  }
}

export default user
