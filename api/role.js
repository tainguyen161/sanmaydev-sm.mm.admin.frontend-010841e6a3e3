import axios from 'axios'

export function getRoles (parameter) {
  const qs = require('qs')
  return axios({
    url: '/sso/api/roles',
    method: 'get',
    data: qs.stringify(parameter),
    headers: process.env.api.user.header
  })
}

export function getRoleByID (id) {
  return axios({
    url: `/sso/api/roles/${id}`,
    method: 'get',
    headers: process.env.api.user.header
  })
}

export function updateRoleByID (id, data) {
  const qs = require('qs')
  if (id) {
    return axios({
      url: `/sso/api/roles/${id}`,
      method: 'patch',
      data: qs.stringify(data),
      headers: process.env.api.user.header
    })
  }

  return axios({
    url: `/sso/api/roles`,
    method: 'post',
    data: qs.stringify(data),
    headers: process.env.api.user.header
  })
}

export function removeRoleByID (id) {
  return axios({
    url: `/sso/api/roles/${id}`,
    method: 'delete',
    headers: process.env.api.user.header
  })
}
