import axios from 'axios'

export function login (parameter) {
  const qs = require('qs')
  parameter.grant_type = 'password'
  return axios({
    url: '/oauth/login',
    method: 'post',
    data: qs.stringify(parameter),
    headers: process.env.api.user.header
  })
}
export function logout () {
  return axios({
    url: '/oauth/logout',
    method: 'post',
    headers: process.env.api.user.header
  })
}
