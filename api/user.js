import axios from 'axios'

export function getUsers (parameter) {
  const qs = require('qs')
  return axios({
    url: '/sso/api/users',
    method: 'get',
    data: qs.stringify(parameter),
    headers: process.env.api.user.header
  })
}

export function getUserByID (id) {
  return axios({
    url: `/sso/api/users/${id}`,
    method: 'get',
    headers: process.env.api.user.header
  })
}

export function updateUserByID (id, data) {
  const qs = require('qs')
  if (id) {
    return axios({
      url: `/sso/api/users/${id}`,
      method: 'patch',
      data: qs.stringify(data),
      headers: process.env.api.user.header
    })
  }

  return axios({
    url: `/sso/api/users`,
    method: 'post',
    data: qs.stringify(data),
    headers: process.env.api.user.header
  })
}

export function removeUserByID (id) {
  return axios({
    url: `/sso/api/users/${id}`,
    method: 'delete',
    headers: process.env.api.user.header
  })
}
