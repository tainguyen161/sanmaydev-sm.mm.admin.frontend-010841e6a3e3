import Vue from 'vue'
import store from '~/store/user'
Vue.directive('permission', {
  inserted (el, binding, vnode) {
    const { value } = binding
    const roles = store.state.permissions
    if (value && Array.isArray(value) && value.length > 0) {
      const permissionRoles = value
      const hasPermission = roles.some((role) => {
        return permissionRoles.includes(role)
      })
      if (!hasPermission) {
        el.parentNode && el.parentNode.removeChild(el)
      }
    } else {
      throw new Error(`need roles! Like v-permission="['admin','editor']"`)
    }
  }
})
