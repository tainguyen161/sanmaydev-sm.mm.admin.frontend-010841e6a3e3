import createPersistedState from 'vuex-persistedstate'

export default ({ store }) => {
  window.onNuxtReady(() => {
    createPersistedState({
      key: 'mymarket',
      paths: ['user.info', 'user.permissions']
    })(store)
  })
}
